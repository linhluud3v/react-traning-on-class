import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';


function Alo(props) {

    const [state, setState] = React.useState(false);

    React.useEffect(() => {
        setTimeout(() => setState(prev => !prev),5000)
    }, [])
    return (
        <div>
            Alo Page
            {state && <Redirect to="/" />}
        </div>
    );
}

export default Alo;