import React from 'react';
import { Button } from 'react-bootstrap';

function Abc (props) {
  return (
    <div>
      <h3>{props.title}</h3>
      <p>{props.body}</p>
      <Button variant="primary" onClick={props.deleteAction}>Xóa Post</Button>
    </div>
  )
}
export default Abc;


