import React from 'react';
import PostItem from '../PostItem';
import { Container, Row, Col } from 'react-bootstrap';
import { getPosts, deletePost } from '../../request';
import { connect } from 'react-redux';
import reducer from '../../store/reducer';
import { createStore } from 'redux';


function Main (props) {
  const initialData = [{id: 1, title: 'alo'}, {id: 2, title: 'xyz'}, {id: 3, title: 'ggg'}];
  const [state, setState] = React.useState(initialData);
  // const state = store.getState();
  React.useEffect(() => {
    getPosts().then(result => {
      console.log(result);
      return setState(result.data);
    });
  });
  debugger;
console.log(state);
  const store = createStore(reducer);
  debugger;
  return (
  <Container>
    <Row>
      <Col>
      <h2>{props.posts}</h2>
      <button onClick ={() => props.bopMeoState()}>bop meo</button>
    </Col>
      {state.map(({id, ...theRest}) => (
        <Col sm={4}  key={id}>
          <PostItem {...theRest} deleteAction={() => deletePost()} />
        </Col>)
      )}
    </Row>
  </Container>
  )
}

const mapStateToProps = state => {
  return{
    posts: state.text,
    quantity: state.quantity

  }
}

const mapDispatchToProps = dispatch => {
  return{
    bopMeoState: () => dispatch({type: 'BUOI_CHIEU', })
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Main)