import React from 'react';
import PostList from '../../components/PostList';
// import Alo from '../../components/Alo';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path='/' render={() => <h2>Home Page</h2>} />
          <Route path='/post' component={PostList} />
          <Route render={() => <h2>Not Found Page</h2>} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
